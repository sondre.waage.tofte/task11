﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11a
{
    class Cash : Payment
    {
        public int Youowe1 { get; set; }
        public int Cashm { get; set; }

        public Cash(int youowe1, int cashm)
        {
            Youowe1 = youowe1;
            Cashm = cashm;
        }
        public override void Pay()
        {
            int change = Cashm - Youowe1;
            if (Youowe1 <= Cashm)
            {
                Console.WriteLine("You paid {0} with {1} cash, and got {2} in change", Youowe1, Cashm, change);
            }
            else
            {
                Console.WriteLine("You do not have enough money");
            }
        }
        public override void GetInfo()
        {
            Console.WriteLine("You owe {0} and want to pay with {1} cash", Youowe1, Cashm);
        }
    }
}
