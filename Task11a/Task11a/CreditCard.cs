﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11a
{
    class CreditCard : Card
    {
        public int Youowe2 { get; set; }
        public int Oncaccount { get; set; }

        public CreditCard(int youowe2, int oncaccount)
        {
            Youowe2 = youowe2;
            Oncaccount = oncaccount;
            // skriv hva de har i kredit altså i minus.
        }
        public override void Pay()
        {
            Oncaccount = Oncaccount - Youowe2;
            int Credit = -Oncaccount;
            if (Oncaccount >= 0)
            {
                Console.WriteLine("You paid, {0} from your credit account. You got {1} left on your creditcard", Youowe2, Oncaccount);
            }
            else
            {
                Console.WriteLine("You paid, {0} from your credit account. You got {1} in credit", Youowe2, Credit);
            }
            
        }
        public override void GetInfo()
        {
            Console.WriteLine("You want to pay {0} from your savingsaccount. You got {1} on your creditcard", Youowe2, Oncaccount);
        }
    }
}
