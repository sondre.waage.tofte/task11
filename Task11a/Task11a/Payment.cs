﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11a
{
    abstract class Payment
    {

        public abstract void GetInfo();
        public abstract void Pay();
    }
}
