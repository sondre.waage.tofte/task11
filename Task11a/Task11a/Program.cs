﻿using System;

namespace Task11a
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("How do you want to pay? cash,creditcard or savingscard");
            string PaymentMethod = Console.ReadLine();

            if (PaymentMethod == "cash")
            {
                Console.WriteLine("How much do you want to pay?");
                int o = Convert.ToInt16(Console.ReadLine());
                Console.WriteLine("How much cash do you have?");
                int c = Convert.ToInt16(Console.ReadLine());
                Payment p = new Cash(o, c);
                p.GetInfo();
                p.Pay();
            }
            else if (PaymentMethod == "creditcard")
            {
                Console.WriteLine("How much do you want to pay?");
                int o = Convert.ToInt16(Console.ReadLine());
                Console.WriteLine("How much do you have on your account?");
                int ca = Convert.ToInt16(Console.ReadLine());
                Payment p = new CreditCard(o, ca);
                p.GetInfo();
                p.Pay();
            }
            else if (PaymentMethod == "savingscard")
            {
                Console.WriteLine("How much do you want to pay?");
                int o = Convert.ToInt16(Console.ReadLine());
                Console.WriteLine("How much do you have on your account?");
                int sa = Convert.ToInt16(Console.ReadLine());
                Payment p = new Savingscard(o, sa);
                p.GetInfo();
                p.Pay();
            }
            else
            {
                Console.WriteLine("Try again");
            }
            /*Payment[] Payments = { new Cash(88, 100), new Savingscard(77, 133), new CreditCard(69, 201) };

            foreach (Payment p in Payments)
            {
                p.Pay();
                p.GetInfo();
            }*/
        }
    }
}
