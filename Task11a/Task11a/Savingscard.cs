﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11a
{
    class Savingscard : Card
    {
        public int Youowe3 { get; set; }
        public int Onsaccount { get; set; }

        public Savingscard (int youowe3, int onsaccount)
        {
            Youowe3 = youowe3;
            Onsaccount = onsaccount;
        }
        public override void Pay()
        {
            if (Youowe3 <= Onsaccount)
            {
                Onsaccount = Onsaccount - Youowe3;
                Console.WriteLine("You paid, {0} from your savings account. You got {1} left on your savingsaccount", Youowe3, Onsaccount);
            }
            else
            {
                Console.WriteLine("You do not have enough money");
            }
        }
        public override void GetInfo()
        {
            Console.WriteLine("You want to pay {0} from your savingsaccount. You got {1} on your savingsaccount", Youowe3, Onsaccount);
        }
    }
}
